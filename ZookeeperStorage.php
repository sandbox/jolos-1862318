<?php

/**
 * @file
 * Contains Drupal\Core\Config\ZookeeperStorage.
 */

namespace Drupal\Core\Config;

use Drupal\Component\Utility\NestedArray;

/**
 * Defines the zookeeper storage controller.
 * 
 * This storage controller will try to read the
 * configuration from zookeeper, if the configuration is
 * not available in zookeeper, it will read from the default 
 * storage and load this configuration into zookeeper.
 * This implementation is currently very unstable 
 * ( read: doesn't work most of the time )
 * This needs the zookeeper php extension to work
 * You also need to take care of the loading yourself 
 * ( i.e. hack CoreBundle.php )
 */
class ZookeeperStorage implements StorageInterface {

  /**
   * The configuration storage to be cached.
   *
   * @var Drupal\Core\Config\StorageInterface
   */
  protected $storage;

  /**
   * Constructs a new ZookeeperStorage controller.
   *
   * @param Drupal\Core\Config\StorageInterface $storage
   *   A configuration storage controller to be cached.
   * @param Drupal\Core\Cache\CacheBackendInterface $cache
   *   A cache backend instance to use for caching.
   */
  public function __construct(StorageInterface $storage, $zookeeper) {
    $this->storage = $storage;
    $this->zk =  new \Zookeeper($zookeeper);
  }

  /**
   * Implements Drupal\Core\Config\StorageInterface::exists().
   */
  public function exists($name) {
    // The cache would read in the entire data (instead of only checking whether
    // any data exists), and on a potential cache miss, an additional storage
    // lookup would have to happen, so check the storage directly.
    return $this->storage->exists($name);
  }

  /**
   * Implements Drupal\Core\Config\StorageInterface::read().
   */
  public function read($name) {
    // check if zookeeper node exists
    $path = "/" . str_replace('.', '/', $name);

    if ($this->zk->exists($path)) {
      $tree = array();
      $this->readZkTree($path, $tree);
      $parts = explode('/', $path);
      array_shift($parts);
      $tree = NestedArray::getValue($tree, $parts);
      /*
      // this snippet helps can help to catch
      // unwanted differences with the default storage
      $data = $this->storage->read($name);
      $diff = array_diff($tree,$data);
      if (!empty($diff)) {
        var_dump($name);
        var_dump($diff);
        var_dump($data);
      } */
      return $tree;
    }
    else {
      $data = $this->storage->read($name);
      //var_dump($data);
      $nestedarray = array();
      $parts = explode('/', $path);
      array_shift($parts);
      NestedArray::setValue($nestedarray, $parts, $data);
      $this->writeZkTree('/', $nestedarray);
      return $data;
    }
  }

  /**
   * Recursively read a conf array from zk
   */
  public function readZkTree($path, &$tree) {
    $children = $this->zk->getChildren($path);
    if (count($children) > 0) {
      foreach ($children as $child) {
       $childpath = "$path/$child";
       $this->readZkTree($childpath, $tree);
      } 
    }
    else {
      $data = $this->zk->get($path);
      $pathar = explode('/', $path);
      array_shift($pathar);
      if (!empty($data)) {
        NestedArray::setValue($tree, $pathar, $data); 
      }
    }
  }

  /**
   * Recursively write a conf array to zk
   */
  public function writeZkTree($zkhead, &$head) {
    if (is_array($head)){
      // we dive deeper into the tree
      foreach($head as $node => $value) {
        if ($zkhead == '/') {
          $path = "/$node";
        }
        else {
          $path = "$zkhead/$node";
        }

        if (!$this->zk->exists($path)) {
          $this->createNode($path, "");
        }
        $this->writeZkTree($path, $value);
      }
    }
    else if(isset($head)) {
      // we reached the leafs of the tree, save value
      $this->zk->set($zkhead, $head);
    }
  }

  /**
   * Helper function to create a zk node
   */
  public function createNode($path, $val) {
    $params = array(
        array(
          'perms' => \Zookeeper::PERM_ALL,
          'scheme' => 'world',
          'id' => 'anyone',
        )
     );
    $this->zk->create($path, $val, $params);
  }

  /**
   * Implements Drupal\Core\Config\StorageInterface::write().
   */
  public function write($name, array $data) {
    if ($this->storage->write($name, $data)) {
      $path = "/" . str_replace('.', '/', $name);
      $nestedarray = array();
      $parts = explode('/', $path);
      array_shift($parts);
      NestedArray::setValue($nestedarray, $parts, $data);
      $this->writeZkTree('/', $nestedarray);
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Implements Drupal\Core\Config\StorageInterface::delete().
   */
  public function delete($name) {
    // If the cache was the first to be deleted, another process might start
    // rebuilding the cache before the storage is gone.
    if ($this->storage->delete($name)) {
      $path = "/" . str_replace('.', '/', $name);
      $this->zk->delete($path);
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Implements Drupal\Core\Config\StorageInterface::rename().
   */
  public function rename($name, $new_name) {
    // If the cache was the first to be deleted, another process might start
    // rebuilding the cache before the storage is renamed.
    if ($this->storage->rename($name, $new_name)) {
      $path = "/" . str_replace('.', '/', $name);
      $this->zk->delete($name);
      $this->zk->delete($new_name);
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Implements Drupal\Core\Config\StorageInterface::encode().
   */
  public function encode($data) {
    return $this->storage->encode($data);
  }

  /**
   * Implements Drupal\Core\Config\StorageInterface::decode().
   */
  public function decode($raw) {
    return $this->storage->decode($raw);
  }

  /**
   * Implements Drupal\Core\Config\StorageInterface::listAll().
   *
   * Not supported by CacheBackendInterface.
   */
  public function listAll($prefix = '') {
    return $this->storage->listAll($prefix);
  }
}
